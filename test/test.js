var assert = require('assert');
var TraceCoin = require('../lib/coin');

TraceCoin("localhost:3001", "xyHyJphMh5HqWVgg7Qw3CKk6TtWZwMGQ");

var currentUser = "";
var recUser = "";
var currentSystemTrx = "";
var currentUserToUserTrx = "";
var paypalLocalInvoiceId = "";
var cardToken = "";

describe('Users', function () {
  describe('create', function () {
    it('User creation', function (done) {
      TraceCoin.users.create(function (success, err, data) {
        currentUser = data;
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
  describe('balance', function () {
    it('User balance retrieval', function (done) {
      TraceCoin.users.getBalance(currentUser, function (success, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
  describe('create 2nd user', function () {
    it('2nd User creation', function (done) {
      TraceCoin.users.create(function (success, err, data) {
        recUser = data;
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
  describe('Profit trxs', function () {
    it('Retrieval', function (done) {
      TraceCoin.users.getProfitTransactions(recUser ,function (success, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
});

describe('System transactions', function () {
  describe('create', function () {
    it('Trx personal creation', function (done) {
      TraceCoin.systemTransactions.create(currentUser, 1000, true, TraceCoin.systemTransactions.Type.Personal, function (success, err, data) {
        currentSystemTrx = data;
        assert.notStrictEqual(data, null);
        TraceCoin.users.getBalance(currentUser, function (success, err, data) {
          assert.strictEqual(data.balance, 1000);
          done();
        });
      });
    });
    it('Trx profit creation', function (done) {
      TraceCoin.systemTransactions.create(currentUser, 1000, true, TraceCoin.systemTransactions.Type.Profit, function (success, err, data) {
        assert.notStrictEqual(data, null);
        TraceCoin.users.getBalance(currentUser, function (success, err, data) {
          assert.strictEqual(data.profit_balance, 1000);
          done();
        });
      });
    });
    it('Trx combined withdraw', function (done) {
      TraceCoin.systemTransactions.create(currentUser, 2000, false, TraceCoin.systemTransactions.Type.Combined, function (success, err, data) {
        assert.notStrictEqual(data, null);
        TraceCoin.users.getBalance(currentUser, function (success, err, data) {
          assert.strictEqual(data.balance, 0);
          assert.strictEqual(data.profit_balance, 0);
          done();
        });
      });
    });
    it('Trx personal creation', function (done) {
      TraceCoin.systemTransactions.create(currentUser, 1000, true, TraceCoin.systemTransactions.Type.Personal, function (success, err, data) {
        assert.notStrictEqual(data, null);
        TraceCoin.users.getBalance(currentUser, function (success, err, data) {
          assert.strictEqual(data.balance, 1000);
          done();
        });
      });
    });
    it('Trx profit creation', function (done) {
      TraceCoin.systemTransactions.create(currentUser, 1000, true, TraceCoin.systemTransactions.Type.Profit, function (success, err, data) {
        assert.notStrictEqual(data, null);
        TraceCoin.users.getBalance(currentUser, function (success, err, data) {
          assert.strictEqual(data.profit_balance, 1000);
          done();
        });
      });
    });
  });
  describe('getInformation', function () {
    it('Trx information', function (done) {
      TraceCoin.systemTransactions.getInformation(currentSystemTrx, function (success, err, data) {
        assert.strictEqual(data.amount, 1000);
        done();
      });
    });
  });
  describe('refunds', function () {
    it('Trx refund', function (done) {
      TraceCoin.systemTransactions.refund(currentSystemTrx, TraceCoin.systemTransactions.RefundType.Original, function (success, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
});

describe('User to User transactions', function () {
  describe('create', function () {
    it('Trx creation personal', function (done) {
      TraceCoin.userToUserTransactions.create(currentUser, recUser, 1000, TraceCoin.userToUserTransactions.Type.Personal, function (success, err, data) {
        currentUserToUserTrx = data;
        assert.notStrictEqual(data, null);
        TraceCoin.users.getBalance(recUser, function (success, err, data) {
          assert.strictEqual(data.balance, 1000);
          done();
        });
      });
    });
  });
  describe('getInformation', function () {
    it('Trx information', function (done) {
      TraceCoin.userToUserTransactions.getInformation(currentUserToUserTrx, function (success, err, data) {
        assert.strictEqual(data.amount, 1000);
        done();
      });
    });
  });
  describe('create', function () {
    it('Trx creation profit', function (done) {
      TraceCoin.userToUserTransactions.create(currentUser, recUser, 1000, TraceCoin.userToUserTransactions.Type.Profit, function (success, err, data) {
        currentUserToUserTrx = data;
        assert.notStrictEqual(data, null);
        TraceCoin.users.getBalance(recUser, function (success, err, data) {
          assert.strictEqual(data.balance, 1000);
          done();
        });
      });
    });
  });
  describe('getInformation', function () {
    it('Trx information', function (done) {
      TraceCoin.userToUserTransactions.getInformation(currentUserToUserTrx, function (success, err, data) {
        assert.strictEqual(data.amount, 1000);
        done();
      });
    });
  });
});

describe('Financial external transactions', function () {
  describe('PayPayl create', function () {
    it('Trx creation', function (done) {
      TraceCoin.financialExternalTransactions.paypalCreate(currentUser, "http://localhost", "http://localhost", 100, "PAYPAL TRACE", "MXN", 100, "TEST-INVOICE", function (sucess, err, data) {
        assert.notStrictEqual(data, null);
        paypalLocalInvoiceId = data.localInvoiceId;
        done();
      });
    });
  });
  describe('History', function () {
    it('Trx history', function (done) {
      TraceCoin.financialExternalTransactions.transactionsHistory([TraceCoin.financialExternalTransactions.Services.PayPal], "2021-01-01", "2021-11-01", 
          [TraceCoin.financialExternalTransactions.Status.Pending, TraceCoin.financialExternalTransactions.Status.Completed], function (sucess, err, data) {
        assert.notStrictEqual(data, null);
        paypalLocalInvoiceId = data[0].local_invoice_id;
        done();
      });
    });
  });
  describe('Fetch Trxs', function () {
    it('Trx fetch', function (done) {
      TraceCoin.financialExternalTransactions.fetchTransactions(null, null, paypalLocalInvoiceId, null, null, null, null, function (sucess, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
  // describe('OpenPay', function () {
  //   it('Store charge', function (done) {
  //     TraceCoin.financialExternalTransactions.openpayStoreCharge(currentUser, 667, "Fondeo de monedas para sistema OPENPAY, pago en tienda", "MXN", 300, "FET-API-KEY-OPENPAY-STORE", "alywjxa9xgvfbwbtjele", function (sucess, err, data) {
  //       assert.notStrictEqual(data, null);
  //       done();
  //     });
  //   });
  // });
  describe('PayCIPS', function () {
    it('Card tokenization', function (done) {
      this.timeout(5000)
      TraceCoin.financialExternalTransactions.paycipsTokenizeCard("4917988912987740", "703", "1223", "Test1", "", "smithvictor360@gmail.com", "0.0.0.0", "Rinconda de Bejar 1755", "", "Jalisco", "Mexico", "45079", function (sucess, err, data) {
        assert.notStrictEqual(data, null);
        cardToken = data;
        done();
      });
    });
    it('Card token payment', function (done) {
      this.timeout(5000)
      TraceCoin.financialExternalTransactions.paycipsTokenPayment(currentUser, cardToken, "hola_callate@hotmail.com", "0.0.0.0", 1000, "Fondeo PayCIPS", 300, "FET-API-PAYCIPS-TESTING-FRAMEWORK", function (sucess, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
    it('Store charge', function (done) {
      this.timeout(5000)
      TraceCoin.financialExternalTransactions.paycipsStoreCharge(currentUser, 667, "Fondeo de monedas para sistema PayCIPS, pago en tienda", 300, "FET-API-KEY-PAYCIPS-STORE", function (sucess, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
});

describe('Users', function () {
  describe('transactions/combined/', function () {
    it('User combined transactions', function (done) {
      TraceCoin.users.getCombinedTransactions(currentUser, function (success, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
  describe('transactions/fet/', function () {
    it('User external transactions', function (done) {
      TraceCoin.users.getFETransactions(currentUser, function (success, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });
});


describe('Stats', function () {
  describe('stats/balance-summary', function () {
    it('Balance Summary', function (done) {
      TraceCoin.stats.getBalanceSummary(function (success, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });

  describe('stats/withdraw-summary', function () {
    it('Withdraw Summary', function (done) {
      TraceCoin.stats.getWithdrawSummary(function (success, err, data) {
        assert.notStrictEqual(data, null);
        done();
      });
    });
  });

});