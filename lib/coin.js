const urllib = require('urllib');

TraceCoin.SERVER_URL = "";
TraceCoin.API_KEY  = "";

function TraceCoin(serverURL, apiKey){

    TraceCoin.SERVER_URL = serverURL;
    TraceCoin.API_KEY = apiKey;

}

TraceCoin.users = new Users();
TraceCoin.systemTransactions = new SystemTransactions();
TraceCoin.userToUserTransactions = new UserToUserTransactions();
TraceCoin.financialExternalTransactions = new FinancialExternalTransactions();
TraceCoin.stats = new Stats();
TraceCoin.healthCheck = new HealthCheck();

function Users(){
    var endPointURL = "/users";

    /**
     * Creates a new user on the database and returns its token string to identify it externally
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.create = function(callback){
        const operation = "/create";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', null, callback);
    }
    
    /**
     * Retrieves a user balance in local currency
     * @param {string} userToken - User external token 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.getBalance = function(userToken ,callback){
        const operation = "/balance/" + userToken;
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }

    /**
     * Retrieves a user combined transactions (user to user and system transactions)
     * @param {string} userToken - User external token 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.getCombinedTransactions = function(userToken ,callback){
        const operation = "/transactions/combined/" + userToken;
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }

    /**
     * Retrieves a user profit transactions (system transactions)
     * @param {string} userToken - User external token 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.getProfitTransactions = function(userToken ,callback){
        const operation = "/transactions/profit/" + userToken;
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }

    /**
     * Retrieves a user financial external transactions
     * @param {string} userToken - User external token 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.getFETransactions = function(userToken ,callback){
        const operation = "/transactions/fet/" + userToken;
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }

}

function SystemTransactions(){
    var endPointURL = "/systemtrxs";

    this.Type = {
        Personal : 1,
        Profit : 2,
        Combined : 3
    }

    this.RefundType = {
        Original : 1,
        Profit : 2,
        Personal : 3
    }

    /**
     * Creates a new user on the database and returns its token string to identify it externally
     * @param {string} userToken - User external token
     * @param {number} amount - Amount of local currency to be applied to transaction
     * @param {boolean} isSystemPaying - Flag reflecting whether the funds will be provided by the system or the user
     * @param {number} type - Trx type
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.create = function(userToken, amount, isSystemPaying, type, callback){
        const operation = "/create";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
                                            "userToken" : userToken,
                                            "amount" : amount,
                                            "isSystemPaying" : isSystemPaying,
                                            "type" : type
                                        }, callback);
    }

    /**
     * Retrieves a system transaction details
     * @param {string} trxToken - Transaction external token 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.getInformation = function(trxToken ,callback){
        const operation = "/information/" + trxToken;
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }

    /**
     * Creates a new transaction to refund the specified system trx
     * @param {string} trxToken System transaction token
     * @param {RefundType} destination Refund type of new trx
     * @param {Function} callback Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.refund = function(trxToken, destination, callback){
        const operation = "/refund";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
                                            "trxToken" : trxToken,
                                            "destination" : destination
                                        }, callback);
    }

}

function UserToUserTransactions(){
    var endPointURL = "/userusertrxs";

    this.Type = {
        Personal : 1,
        Profit : 2
    }

    /**
     * Creates a new user on the database and returns its token string to identify it externally
     * @param {string} senderToken - Sender User external token
     * @param {string} receiverToken - Receiver User external token
     * @param {number} amount - Amount of local currency to be applied to transaction
     * @param {number} balanceType - Type of balance to be transferred 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.create = function(senderToken, receiverToken, amount, balanceType, callback){
        const operation = "/create";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
                                            "senderToken" : senderToken,
                                            "receiverToken" : receiverToken,
                                            "amount" : amount,
                                            "balanceType" : balanceType
                                        }, callback);
    }

    /**
     * Retrieves a user to user transaction details
     * @param {string} trxToken - Transaction external token 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.getInformation = function(trxToken ,callback){
        const operation = "/information/" + trxToken;
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }

}

function FinancialExternalTransactions(){
    var endPointURL = "/finexttrxs";

    this.Status = {
        Started : 1,
        Pending : 2,
        Error : 3,
        Completed : 4,
        Cancelled : 5,
        Expired: 6
    }

    this.Types = {
        Withdrawal : 1,
        Funding : 2
    }

    this.Services = {
        PayPal : 1,
        OpenPay : 2
    }

    /**
     * Creates a new order via PayPal and returns the approval link for UI display
     * @param {string} userToken - User external token
     * @param {string} returnURL - Return URL parameter for PayPal callback
     * @param {string} cancelURL - Cancel URL parameter for PayPal callback
     * @param {number} localCurrency - Amount of "coins" that the transaction will be valid for
     * @param {string} description - Transaction description, text will be used also for PayPal transaction description
     * @param {string} currency - External currency ISO code
     * @param {number} amount - Amount of external currency which will be billed via PayPal
     * @param {string} localInvoiceId - Local invoice external token for external ID purposes
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.paypalCreate = function(userToken, returnURL, cancelURL, localCurrency, description, currency, amount, localInvoiceId, callback){
        const operation = "/paypal/create";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
            "userToken" :  userToken,
            "returnURL" :  returnURL,
            "cancelURL" :  cancelURL,
            "localCurrency" :  localCurrency,
            "description" :  description,
            "currency" :  currency,
            "amount" :  amount,
            "localInvoiceId" :  localInvoiceId
        }, callback);
    }

    /**
     * Process a PayPal transaction that has been previously authorized 
     * @param {string} userToken - User external token
     * @param {string} token - PayPal authorization callback token
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.paypalProcess = function(userToken, token, callback){
        const operation = "/paypal/process";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
                                            "userToken" : userToken,
                                            "token" : token
                                        }, callback);
    }

    /**
     * Process a pending withdrawal user external trx
     * @param {string} localInvoiceId - Local invoice ID for trx
     * @param {string} trxId - Invoice ID for record purposes for external banking trx
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.completeTrx = function(localInvoiceId, trxId, callback){
        const operation = "/complete/trx";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
                                            "localInvoiceId" : localInvoiceId,
                                            "trxId" : trxId
                                        }, callback);
    }

    /**
     * Process a pending withdrawal user external trx
     * @param {string} localInvoiceId - Local invoice ID for trx
     * @param {string} reason - Reason description of trx cancellation
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.cancelTrx = function(localInvoiceId, reason, callback){
        const operation = "/cancel/trx";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
                                            "localInvoiceId" : localInvoiceId,
                                            "reason" : reason
                                        }, callback);
    }

    /**
     * Process a new order via OpenPay and executes it
     * @param {string} userToken - User external token
     * @param {number} localCurrency - Amount of "coins" that the transaction will be valid for
     * @param {string} description - Transaction description, text will be used also for OpenPay transaction description
     * @param {string} currency - External currency ISO code
     * @param {number} amount - Amount of external currency which will be billed via OpenPay
     * @param {string} localInvoiceId - Local invoice external token for external ID purposes
     * @param {string} opCustomerToken - OpenPay Customer ID
     * @param {string} opToken - OpenPay token obtained via UI transaction
     * @param {string} deviceSesId - Device session ID provided by OpenPay UI module for anti-scam purposes
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.openpayProcess = function(userToken, localCurrency, description, currency, amount, localInvoiceId, opCustomerToken, opToken, deviceSesId, callback){
        const operation = "/openpay/process";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
            "userToken" :  userToken,
            "localCurrency" :  localCurrency,
            "description" :  description,
            "currency" :  currency,
            "amount" :  amount,
            "localInvoiceId" :  localInvoiceId,
            "opCustomerToken" :  opCustomerToken,
            "opToken" :  opToken,
            "deviceSesId" :  deviceSesId
        }, callback);
    }

    /**
     * Creates a new openpay store charge for a registered customer
     * @param {string} userToken - User external token
     * @param {number} localCurrency - Amount of "coins" that the transaction will be valid for
     * @param {string} description - Transaction description, text will be used also for OpenPay transaction description
     * @param {string} currency - External currency ISO code
     * @param {number} amount - Amount of external currency which will be billed via OpenPay
     * @param {string} localInvoiceId - Local invoice external token for external ID purposes
     * @param {string} opCustomerToken - OpenPay Customer ID
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.openpayStoreCharge = function(userToken, localCurrency, description, currency, amount, localInvoiceId, opCustomerToken, callback){
        const operation = "/openpay/store/charge";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
            "userToken" :  userToken,
            "localCurrency" :  localCurrency,
            "description" :  description,
            "currency" :  currency,
            "amount" :  amount,
            "localInvoiceId" :  localInvoiceId,
            "opCustomerToken" :  opCustomerToken
        }, callback);
    }

    /**
     * Creates a new PayCIPS store charge
     * @param {string} userToken - User external token
     * @param {number} localCurrency - Amount of "coins" that the transaction will be valid for
     * @param {string} description - Transaction description, text will be used also for OpenPay transaction description
     * @param {number} amount - Amount of external currency which will be billed via OpenPay
     * @param {string} localInvoiceId - Local invoice external token for external ID purposes
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.paycipsStoreCharge = function(userToken, localCurrency, description, amount, localInvoiceId, callback){
        const operation = "/paycips/store/charge";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
            "userToken" :  userToken,
            "localCurrency" :  localCurrency,
            "description" :  description,
            "amount" :  amount,
            "localInvoiceId" :  localInvoiceId
        }, callback);
    }

    /**
     * Withdraws funds from a account to a third party account
     * @param {string} userToken - User external token
     * @param {number} localCurrency - Amount of "coins" that the transaction will be valid for
     * @param {string} description - Transaction description, text will be used also for PayPal transaction description
     * @param {number} currency - External currency ISO code
     * @param {number} amount - Amount of external currency which will be billed via PayPal
     * @param {string} localInvoiceId - Local invoice external token for external ID purposes
     * @param {string} opUserId - Open pay user id
     * @param {string} clabeId - CLABE bank account
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.openpayWithdraw = function(userToken, localCurrency, description, currency, amount, localInvoiceId, clabeId, opUserId, callback){
        const operation = "/openpay/withdraw";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
            "userToken" :  userToken,
            "localCurrency" :  localCurrency,
            "description" :  description,
            "currency" :  currency,
            "amount" :  amount,
            "localInvoiceId" :  localInvoiceId,
            "clabeId" :  clabeId,
            "opUserId" :  opUserId,
        }, callback);
    }

    /**
     * Creates a token for an specific card on the PayCIPS system for further use on payments authorization
     * @param {string} ccNumber Credit card number
     * @param {string} cvv CVV code from credit card
     * @param {string} expDate Expiration date from credit card on format MMYY
     * @param {string} firstName Firstname from credit card holder, full name can also be passed on this parameter
     * @param {string} lastName Last name from credit card holder, if null please input an empty string ""
     * @param {string} email Customer email
     * @param {string} customerIP Customer IP address
     * @param {string} street Billing address street line 1
     * @param {string} street2 Billing address street line 2
     * @param {string} state Billing address state
     * @param {string} country Billing address country
     * @param {string} zipCode Billing address Zip Code
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.paycipsTokenizeCard = function(ccNumber, cvv, expDate, firstName, lastName, email, customerIP, street, street2, state, country, zipCode, callback){
        const operation = "/paycips/tokenizeCard";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
            "ccNumber" : ccNumber,
            "cvv" : cvv,
            "expDate" : expDate,
            "firstName" : firstName,
            "lastName" : lastName,
            "email" : email,
            "customerIP" : customerIP,
            "street" : street,
            "street2" : street2,
            "state" : state,
            "country" : country,
            "zipCode" : zipCode
        }, callback);
    }

    /**
     * Creates a trx for a previouly saved card token and adds the balance to its account if the trx is successful
     * @param {string} userToken User identification token on coin server
     * @param {string} cardToken Tokenized card ID
     * @param {string} email Customer email
     * @param {string} customerIP Customer IP address
     * @param {number} localCurrency Coin amount to be added to personal balance account
     * @param {string} description Trx description for customer purposes and registry
     * @param {number} amount Amount of money to be charged on the card
     * @param {string} localInvoiceId Local invoice id for external registry 
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.paycipsTokenPayment = function(userToken, cardToken, email, customerIP, localCurrency, description, amount, localInvoiceId, callback){
        const operation = "/paycips/tokenPayment";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
            "userToken" :  userToken,
            "cardToken" :  cardToken,
            "email" :  email,
            "customerIP" :  customerIP,
            "localCurrency" :  localCurrency,
            "description" :  description,
            "amount" :  amount,
            "localInvoiceId" :  localInvoiceId
        }, callback);
    }

    /**
     * Gets the transactions history for financial external transactions
     * @param {Array} services Service int array for filtering
     * @param {Date} from Start date for filtering
     * @param {Date} to End date for filtering
     * @param {Array} status Status int array for filtering
     * @param {Function} callback Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.transactionsHistory = function(services, from, to, status, callback){
        const operation = "/history";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'POST', {
                                    "services" :  services,
                                    "from" : from,
                                    "to" : to,
                                    "status" : status
                                }, callback);
    }

    /**
     * Gets the transactions matching the search criteria
     * @param {number} id Financial external transaction ID
     * @param {string} serviceId External service ID
     * @param {string} localId Invoice local ID
     * @param {string} status Status ID
     * @param {string} service Service ID
     * @param {string} type Type ID
     * @param {string} user User external Token
     * @param {Function} callback Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.fetchTransactions = function(id, serviceId, localId, status, service, type, user, callback){
        const operation = "/fetch/trxs";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        var requestData = {};
        if(id != null){
            requestData.id = id;
        }
        if(serviceId != null){
            requestData.serviceId = serviceId;
        }
        if(localId != null){
            requestData.localId = localId;
        }
        if(status != null){
            requestData.status = status;
        }
        if(service != null){
            requestData.service = service;
        }
        if(type != null){
            requestData.type = type;
        }
        if(user != null){
            requestData.user = user;
        }
        sendRequestToServer(url, 'POST', requestData, callback);
    }
}

function Stats(){
    var endPointURL = "/stats";

    /**
     * Retrieves the balance summary for all users
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
    this.getBalanceSummary = function(callback){
        const operation = "/balance-summary";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }

    /**
     * Retrieves the withdraw summary for all users
     * @param {Function} callback - Callback function, format (success,err,response,errData) err and response param can be null depending on response
     */
     this.getWithdrawSummary = function(callback){
        const operation = "/withdraw-summary";
        const url = TraceCoin.SERVER_URL + endPointURL + operation;
        sendRequestToServer(url, 'GET', null, callback);
    }
}

function HealthCheck(){
    var endPointURL = "/";

    this.check = function(callback){
        const url = TraceCoin.SERVER_URL + endPointURL;
        sendRequestToServer(url, 'GET', null, callback);
    }
}


var sendRequestToServer = function (url, method, data, callback){
    const urlDestination = url;
    urllib.request(urlDestination,{
        contentType: "json",
        dataType: "json",
        data: data,
        method: method,
        timeout: 5000,
        headers: {
            "Store-Auth" : TraceCoin.API_KEY
        }
    }, function(err, data, res){
        if(err != null || (res.statusCode != 200 &&
            res.statusCode != 201 &&
            res.statusCode != 204)){
            console.log(urlDestination);
            console.log(err);
            callback(false, data != null ? data.error : err, null, data != null ? data.errors : null);
            return;
        }
        callback(true, null, data.response);
    });
}

module.exports = TraceCoin;